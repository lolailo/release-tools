module Project
  autoload :GitlabCe, 'project/gitlab_ce'
  autoload :GitlabEe, 'project/gitlab_ee'
  autoload :OmnibusGitlab, 'project/omnibus_gitlab'

  autoload :Release, 'project/release'
end
