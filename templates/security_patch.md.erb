**General guidelines**

- Be sure to follow the [Security Releases general information](https://gitlab.com/gitlab-org/release/docs/blob/master/general/security/process.md) and [Security Releases as Release Manager](https://gitlab.com/gitlab-org/release/docs/blob/master/general/security/release-manager.md).
- Always work on [https://dev.gitlab.org/](https://dev.gitlab.org/). Do not push anything to [https://gitlab.com](https://gitlab.com)
- Deployment tasks (staging, production canary VMs, and production) should be done only if the patch is for the latest version. Feel free to delete those sections if they don't apply.

## Preparation

- [ ] Picked into respective `stable` branches from the `dev/security` branch. Use the <SECURITY_RELEASE_ISSUE> as a guideline of what MR's should be picked.
- [ ] **Push `ce/<%= version.stable_branch %>` to `dev` only: `git push dev <%= version.stable_branch %>`**
- [ ] **Push `ee/<%= version.stable_branch(ee: true) %>` to `dev` only: `git push dev <%= version.stable_branch(ee: true) %>`**
- [ ] Merge `ce/<%= version.stable_branch %>` into `ee/<%= version.stable_branch(ee: true) %>`

## Packaging

- [ ] **Push `omnibus-gitlab/<%= version.stable_branch %>` to `dev` only: `git push dev <%= version.stable_branch %>`**
- [ ] **Push `omnibus-gitlab/<%= version.stable_branch(ee: true) %>` to `dev` only: `git push dev <%= version.stable_branch(ee: true) %>`**
- [ ] Ping the Security Engineers so they can get started with the blog post. The blog post should also be done on https://dev.gitlab.org/ in a **private snippet**: BLOG_POST_SNIPPET

- [ ] Ensure [tests are green on CE]
- [ ] Ensure [tests are green on EE]

- [ ] Tag the `<%= version.to_patch %>` version using the [`tag` command]:
   ```sh
   # In Slack:
   /chatops run tag --security <%= version.to_patch %>
   ```
- [ ] Check that [EE packages are built] and [CE packages are built]

## Deploy

### staging.gitlab.com

- Staging deploys via the [deployer pipeline](https://ops.gitlab.net/gitlab-com/gl-infra/deployer) happen automatically as soon as the
  [EE packages build](https://dev.gitlab.org/gitlab/omnibus-gitlab/commits/<%= version.to_omnibus(ee: true) %>)
  reaches the `gitlab_com:upload_deploy` stage of the pipeline.

#### QA

- [ ] Create a "QA Task" issue using the ChatOps command:
  ```sh
  # In Slack, replacing LAST_DEPLOYED_VERSION with the appropriate value:
  /chatops run qa_issue --security vLAST_DEPLOYED_VERSION..v<%= version %>
  ```
- [ ] Wait for the QA tasks deadline to pass.

### Canary VMs on gitlab.com

- [ ] Notify #production channel about canary deploy.
- [ ] Deploy `<%= version %>` to the canary VMs on gitlab.com
  ```sh
  # In Slack:
  /chatops run deploy <%= version.to_patch %>-ee.0 --production --canary
  ```

### gitlab.com (production)

- [ ] Get confirmation from a production team member to deploy production. Use `/chatops run oncall production` if needed to find who's on call. If someone besides the oncall confirms, `@mention` the oncall so they are aware.
- [ ] Confirm there are no critical alerts on gitlab.com on the [alerting dashboard](https://dashboards.gitlab.net/d/SOn6MeNmk/alerts)
- [ ] Deploy `<%= version %>` to [GitLab.com]
  ```sh
  # In Slack:
  /chatops run deploy <%= version.to_patch %>-ee.0 --production
  ```

## Release

- [ ] This section should be done in coordination with the Security team, so **make sure to confirm with them before proceeding**
  ```sh
  # In Slack
  @Ethan Strike @James Ritchey - We are ready to publish the security release packages for <%= version %>, please let us know if the blog post is ready.
  ```

- [ ] Publish the packages via ChatOps:
  ```
  # In Slack:
  /chatops run publish <%= version %>
  ```
- [ ] Create the `<%= version %>` version on [version.gitlab.com](https://version.gitlab.com/versions/new?version=<%= version %>). **Be sure to mark it as a security release.**
- [ ] Push `ce/<%= version.stable_branch %>` to all remotes
- [ ] Push `ee/<%= version.stable_branch(ee: true) %>` to all remotes
- [ ] Push `omnibus/<%= version.stable_branch %>` and `omnibus/<%= version.stable_branch(ee: true) %>` to all remotes
- [ ] Push CE, EE and omnibus tags to all remotes
- [ ] Tweet (prepare the Tweet text below or paste the tweet URL instead) in the `#releases` channel:
  ```
  !tweet "GitLab <%= version %> is released! BLOG_POST_URL DESCRIPTION OF THE CHANGES"
  ```
- [ ] Merge the MR's targeting `master` and push to all remotes

---

For references:
- https://dev.gitlab.org/gitlab/gitlab-ee/commits/<%= version.stable_branch(ee: true) %>
- https://dev.gitlab.org/gitlab/gitlabhq/commits/<%= version.stable_branch %>
- https://dev.gitlab.org/gitlab/omnibus-gitlab/commits/<%= version.stable_branch(ee: true) %>
- https://dev.gitlab.org/gitlab/omnibus-gitlab/commits/<%= version.stable_branch %>

[tests are green on CE]: https://dev.gitlab.org/gitlab/gitlabhq/commits/<%= version.stable_branch %>
[tests are green on EE]: https://dev.gitlab.org/gitlab/gitlab-ee/commits/<%= version.stable_branch(ee: true) %>
[EE packages are built]: https://dev.gitlab.org/gitlab/omnibus-gitlab/commits/<%= version.stable_branch(ee: true) %>
[CE packages are built]: https://dev.gitlab.org/gitlab/omnibus-gitlab/commits/<%= version.stable_branch %>

[Omnibus CE stable branch]: https://dev.gitlab.org/gitlab/omnibus-gitlab/commits/<%= version.stable_branch %>
[Omnibus EE stable branch]: https://dev.gitlab.org/gitlab/omnibus-gitlab/commits/<%= version.stable_branch(ee: true) %>

[EE packages are built]: https://dev.gitlab.org/gitlab/omnibus-gitlab/commits/<%= version.stable_branch(ee: true) %>
[CE packages are built]: https://dev.gitlab.org/gitlab/omnibus-gitlab/commits/<%= version.stable_branch %>

[`gitlab/gitlab-ee`]: https://packages.gitlab.com/gitlab/gitlab-ee
[`gitlab/gitlab-ce`]: https://packages.gitlab.com/gitlab/gitlab-ce

[`tag` command]: https://gitlab.com/gitlab-org/release-tools/blob/master/doc/chatops.md#tag
[`security_qa_issue` task]: https://gitlab.com/gitlab-org/release-tools/blob/master/doc%2Frake-tasks.md#security_qa_issuefromtoversion

[staging.gitlab.com]: https://gitlab.com/gitlab-org/takeoff#deploying-gitlab
[GitLab.com]: https://gitlab.com/gitlab-org/takeoff#deploying-gitlab

[publicly acknowledged]: https://about.gitlab.com/vulnerability-acknowledgements/
